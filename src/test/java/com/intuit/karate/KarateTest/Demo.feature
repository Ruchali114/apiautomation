Feature: Karate test for Assignment API

Background: 
	Given url 'http://api.zippopotam.us' 
	Given def readvariable = read ('Demofile.json')
	

 Scenario: Verify in Response.

 	Given path '/de/bw/stuttgart' 
 	When method get 
 	Then status 200 

Scenario: Verify country in Response.

	Given path '/de/bw/stuttgart' 
	When method get 
	Then status 200 
	Then match response.country contains "Germany"
	
	
Scenario: Verify state in Response.

	Given path '/de/bw/stuttgart' 
	When method get 
	Then status 200 
	Then match response.state contains "Baden-Württemberg" 

Scenario: Verify in Response - For Post Code 70597 the place name has Stuttgart Degerloch.

 	Given path '/de/bw/stuttgart' 
 	When method get 
 	Then status 200 
	Then match response == readvariable 
#And match response.place name contains "Stuttgart Degerloch" can not be automated 
#due to spaces in attribute but we can match the values whoes not have spaces like below
#Then match response.places[*].latitude contains ["48.7667"]

Scenario Outline: Verify parameterised request
 
	Given param country = '<country>' 
	And param postal-code = '<postal-code>'
	When method get 
	Then status 200 
	And def res = response 
	And print 'response:', response 
	
Examples: 
		| country | postal-code |
		| us      | 90210       | 
		| us      | 12345       | 
		| can     | B2R         | 