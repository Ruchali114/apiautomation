package com.intuit.karate.KarateTest;
import com.intuit.karate.junit5.*;
public class Runner {

	@Karate.Test
	Karate sample() {
	return Karate.run("Demo").relativeTo(getClass());
	}
}
